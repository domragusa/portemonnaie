#include "totpdialog.hpp"
#include "ui_totpdialog.h"

TOTPDialog::TOTPDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TOTPDialog)
{
    ui->setupUi(this);
}

TOTPDialog::~TOTPDialog()
{
    delete ui;
}

totp_data_t TOTPDialog::getData(){
    totp_data_t tmp;
    tmp.key      = ui->le_seckey->text();
    tmp.interval = ui->sb_interval->value();
    tmp.digits   = ui->sb_digits->value();
    
    return tmp;
}
