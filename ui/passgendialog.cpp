#include "passgendialog.hpp"
#include "ui_passgendialog.h"
#include <QRandomGenerator>
#include <QAction>
#include <QDebug>

QString gen_password(QString pool, int length){
    QString res;
    int pool_length = pool.length();
    if(pool_length == 0) return "";

    auto rand = QRandomGenerator::system();
    for(int i=0; i<length; i++){
        res += pool.at(
            rand->bounded(0, pool_length)
        );
    }

    return res;
}

PassGenDialog::PassGenDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PassGenDialog)
{
    ui->setupUi(this);
    
    QAction *a = ui->le_password->addAction(QIcon::fromTheme("view-refresh"), QLineEdit::TrailingPosition);
    connect(a, &QAction::triggered, this, &PassGenDialog::updatePass);
    
    updatePass();
}

PassGenDialog::~PassGenDialog(){
    delete ui;
}

void PassGenDialog::updatePass(){
    QString pool;
    int pass_len;

    if(ui->cb_uppercase->isChecked()) pool += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if(ui->cb_lowercase->isChecked()) pool += "abcdefghijklmnopqrstuvwxyz";
    if(ui->cb_digits->isChecked())    pool += "0123456789";
    if(ui->cb_symbols->isChecked())   pool += "+/!?%$";
    if(ui->cb_extra->isChecked())     pool += ui->le_extra->text();

    pass_len = ui->hs_length->value();
    password = gen_password(pool, pass_len);

    emit passUpdated(password);
}


QString PassGenDialog::getPass(){
    return password;
}
