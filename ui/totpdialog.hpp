#ifndef DR_TOTPDIALOG_H
#define DR_TOTPDIALOG_H

#include <QDialog>

namespace Ui {
class TOTPDialog;
}

struct totp_data_t{
    QString key;
    int interval;
    int digits;
};

class TOTPDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TOTPDialog(QWidget *parent = nullptr);
    ~TOTPDialog();
    totp_data_t getData();

private:
    Ui::TOTPDialog *ui;
};

#endif
