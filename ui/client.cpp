#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusInterface>
#include <QApplication>
#include <QClipboard>
#include <QMessageBox>
#include <QInputDialog>
#include <QDebug>

#include "passgendialog.hpp"
#include "totpdialog.hpp"
#include "client.hpp"

PMClient::PMClient(QObject *parent, const QString &serviceName) : QObject(parent), iface(nullptr){
    if(!QDBusConnection::sessionBus().isConnected()) return;
    iface = new QDBusInterface(serviceName, "/", "", QDBusConnection::sessionBus());
}

PMClient::~PMClient(){
    delete iface;
}

void PMClient::sync(){
    if(!iface->isValid()) return;
    iface->call("sync");
}

void PMClient::lock(){
    if(!iface->isValid()) return;
    iface->call("lock");
}

void PMClient::unlock(){
    if(!iface->isValid()) return;
    bool ok;
    QString pass = QInputDialog::getText(
        nullptr, "Unlocking password","Password:", QLineEdit::Password, "", &ok);
    
    if(!ok || pass.isEmpty()) return;
    
    iface->call("unlock", pass);
}

void PMClient::changeKey(){
    if(!iface->isValid()) return;
    bool ok;
    QString pass = QInputDialog::getText(
        nullptr, "Unlocking password","Password:", QLineEdit::Password, "", &ok);
    
    if(!ok || pass.isEmpty()) return;
    
    iface->call("changeKey", pass);
}

void PMClient::delPass(const QString &name){
    if(!iface->isValid()) return;
    
    auto response = QMessageBox::question(nullptr, "Confirmation", "Are you sure you want to delete <b>"+name+"</b> password?");
    
    if(response == QMessageBox::Yes){
        QDBusReply<bool> reply = iface->call("delPass", name);
    }
}

void PMClient::getPass(const QString &name){
    if(!iface->isValid()) return;
    
    QDBusReply<QString> reply = iface->call("getPass", name);
    if(reply.isValid()){
        QApplication::clipboard()->setText(reply.value());
    }else{
        QMessageBox::critical(nullptr, "Error", "Password not found!");
    }
}

void PMClient::addPass(const QString &name){
    if(!iface->isValid()) return;
    
    PassGenDialog d;
    int res = d.exec();
    if(res == QDialog::Accepted){
        iface->call("addPass", name, d.getPass());
    }else{
        return;
    }
}

void PMClient::delAuth(const QString &name){
    if(!iface->isValid()) return;
    
    auto response = QMessageBox::question(nullptr, "Confirmation", "Are you sure you want to delete <b>"+name+"</b> authentication token?");
    
    if(response == QMessageBox::Yes){
        QDBusReply<bool> reply = iface->call("delAuth", name);
    }
}

void PMClient::getAuth(const QString &name){
    if(!iface->isValid()) return;
    
    QDBusReply<QString> reply = iface->call("getPass", name);
    if(reply.isValid()){
        QApplication::clipboard()->setText(reply.value());
    }else{
        QMessageBox::critical(nullptr, "Error", "Password not found!");
    }
}

void PMClient::addAuth(const QString &name){
    if(!iface->isValid()) return;
    
    TOTPDialog d;
    int res = d.exec();
    if(res == QDialog::Accepted){
        totp_data_t tmp = d.getData();
        iface->call("addAuth", name, tmp.key, tmp.interval, tmp.digits);
    }
}
