QT       += core gui widgets dbus

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pm
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        main.cpp \
        passgendialog.cpp \
        totpdialog.cpp \
        client.cpp

HEADERS += \
        passgendialog.hpp \
        totpdialog.hpp \
        client.hpp

FORMS += \
        passgendialog.ui \
        totpdialog.ui

target.path = /opt/$${TARGET}/bin

!isEmpty(target.path): INSTALLS += target
