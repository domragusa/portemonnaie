#include <QApplication>
#include <QTimer>
#include <QDebug>

#include "client.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QStringList args = QApplication::arguments();
    PMClient c;
    if(args.size() == 2){
        if(args[1] == "lock"){
            c.lock();
        }else if(args[1] == "unlock"){
            c.unlock();
        }else if(args[1] == "sync"){
            c.sync();
        }else if(args[1] == "change_key"){
            c.changeKey();
        }else{
            return 1;
        }
    }else if(args.size() == 3){
        if(args[1] == "del_pass"){
            c.delPass(args[2]);
        }else if(args[1] == "pass"){
            c.getPass(args[2]);
        }else if(args[1] == "add_pass"){
            c.addPass(args[2]);
        }else if(args[1] == "del_auth"){
            c.delAuth(args[2]);
        }else if(args[1] == "auth"){
            c.getAuth(args[2]);
        }else if(args[1] == "add_auth"){
            c.addAuth(args[2]);
        }else{
            return 1;
        }
    }else{
        return 1;
    }
    
    // we need a delay before exiting because the clipboard needs to be collected by
    // the clipboard manager (if non is present, the delay should be increased to
    // give the user enough time to paste the password or token
    QTimer::singleShot(1000, &a, &QApplication::quit);
    return a.exec();
}
