#ifndef DR_PMClient_HPP
#define DR_PMClient_HPP

#include <QObject>
#include <QtDBus/QtDBus>
#include <QtDBus/QDBusConnection>

class PMClient : public QObject {
    Q_OBJECT
    
private:
    QDBusInterface *iface;
    
public:
    explicit PMClient(QObject *parent = nullptr, const QString &serviceName="io.gitlab.drtiny.test");
    ~PMClient();
    
    void sync();
    void lock();
    void unlock();
    void changeKey();
    
    void delPass(const QString &name);
    void getPass(const QString &name);
    void addPass(const QString &name);
    
    void delAuth(const QString &name);
    void getAuth(const QString &name);
    void addAuth(const QString &name);
};

#endif
