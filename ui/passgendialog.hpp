#ifndef DR_PASSGENDIALOG_HPP
#define DR_PASSGENDIALOG_HPP

#include <QDialog>

namespace Ui {
class PassGenDialog;
}

class PassGenDialog : public QDialog {
    Q_OBJECT

public:
    explicit PassGenDialog(QWidget *parent = nullptr);
    ~PassGenDialog();

public slots:
    void updatePass();
    QString getPass();

signals:
    void passUpdated(QString password);

private:
    Ui::PassGenDialog *ui;
    QString password;
};

#endif
