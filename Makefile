PROJNAME=pm
SOURCES=totp.cpp secstorage.cpp manager.cpp dbus-interface.cpp main.cpp
BUILDDIR=./build
SRCDIR=./daemon
LDLIBS=-lcryptopp -lsystemd

CXX=g++
CXXFLAGS=-O2 -pedantic -Wall -std=c++17
LDFLAGS=-flto
OBJECTS=$(SOURCES:%.cpp=$(BUILDDIR)/%.o)

$(PROJNAME): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(LDLIBS) $^ -o $(BUILDDIR)/$@

$(OBJECTS): $(BUILDDIR)/%.o : $(SRCDIR)/%.cpp
	+$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(BUILDDIR)/*.o
	rm -f $(BUILDDIR)/$(PROJNAME)
