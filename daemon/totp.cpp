#include <ctime>

#include <cryptopp/base32.h>
#include <cryptopp/filters.h>

#include "totp.hpp"

using StringSource = CryptoPP::StringSource;
using StringSink = CryptoPP::StringSink;
using Base32Decoder = CryptoPP::Base32Decoder;

void to_big_endian(const u64_t data, byte buf[8]){
    for(int i=0; i<8; i++){
        buf[i] = (data >> (56-i*8))&0xFF;
    }
}

void from_big_endian(const byte buf[4], u32_t &out){
    out = 0;
    for(int i=0; i<4; i++){
        out |= buf[i]<<(24-i*8);
    }
}

SecByteBlock b32_decode(const std::string &b32_encoded){
    int lookup[256];
    const byte alpha_rfc4648[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    Base32Decoder::InitializeDecodingLookupArray(lookup, alpha_rfc4648, 32, true);
    Base32Decoder decoder;
    auto params = CryptoPP::MakeParameters(CryptoPP::Name::DecodingLookupArray(),(const int *)lookup);
    decoder.IsolatedInitialize(params);
    
    std::string tmp;
    decoder.Attach(new StringSink(tmp));
    decoder.Put((byte*)b32_encoded.data(), b32_encoded.size());
    decoder.MessageEnd();
    
    return SecByteBlock((const byte*)tmp.data(), tmp.size());
}

// very basic power function, it will be used with small exponents
// no need for algorithms better than the naive implementation
int pow10(int n){
    int res = 1;
    for(int i=0; i<n; i++, res *= 10);
    return res;
}

totp::totp(const SecByteBlock &key, const int digits, const int interval) :
    hmac(key, key.size()),
    digits(digits),
    interval(interval)
{}

totp::totp(const totp_data_t &data) :
totp(b32_decode(data.b32_key), data.digits, data.interval)
{}

void totp::calc_hmac(const byte buf[8], byte out[20]){
    hmac.Restart();
    hmac.Update(buf, 8);
    hmac.Final(out);
}

std::string totp::add_padding(const int value){
    std::string tmp = std::to_string(value);
    int diff = digits - tmp.size();
    if(diff>0){
        tmp = std::string(diff, '0') + tmp;
    }
    return tmp;
}

std::string totp::get(){
    byte time_buf[8], hash_buf[20];
    u32_t value;
    
    to_big_endian(std::time(0)/interval, time_buf);
    calc_hmac(time_buf, hash_buf);
    from_big_endian(hash_buf + (hash_buf[19] & 0x0F), value);
    value &= 0x7FFFFFFF;
    
    return add_padding(value%pow10(digits));
}
