#ifndef DR_PASSWORDMANAGER_HPP
#define DR_PASSWORDMANAGER_HPP

#include <string>

#include "secstorage.hpp"

class Manager{
private:
    SecStorage sec_st;
    passmap_t st_pass;
    totpmap_t st_auth;
    bool stale_storage;
    
public:
    bool no_sync;
    Manager(const std::string path);
    
    void sync(bool required=true);
    void init_key(const std::string &password);
    void change_key(const std::string &password);
    
    bool del_pass(const std::string &name);
    bool get_pass(const std::string &name, std::string &data);
    bool add_pass(const std::string &name, const std::string &data);
    
    bool get_auth(const std::string &name, std::string &data);
    bool del_auth(const std::string &name);
    bool add_auth(const std::string &name, const totp_data_t &data);
};

#endif
