#include "manager.hpp"
#include "totp.hpp"

Manager::Manager(const std::string path) : sec_st(path){
}

void Manager::sync(bool required){
    if(required && stale_storage){
        sec_st.store(st_pass, st_auth);
        stale_storage = false;
    }
}

void Manager::init_key(const std::string &password){
    sec_st.derive_key(password);
    sec_st.load(st_pass, st_auth);
}

void Manager::change_key(const std::string &password){
    sec_st.derive_key(password);
    stale_storage = true;
    sync(no_sync);
}

bool Manager::del_pass(const std::string &name){
    auto it = st_pass.find(name);
    if(it == st_pass.end()) return false;

    st_pass.erase(it);
    sync(no_sync);
    return true;
}

bool Manager::get_pass(const std::string &name, std::string &data){
    auto it = st_pass.find(name);
    if(it == st_pass.end()) return false;
    
    data = it->second;
    return true;
}

bool Manager::add_pass(const std::string &name, const std::string &data){
    auto it = st_pass.find(name);
    if(it != st_pass.end()) return false;
    
    st_pass[name] = data;
    stale_storage = true;
    sync(no_sync);
    return true;
}

bool Manager::get_auth(const std::string &name, std::string &data){
    auto it = st_auth.find(name);
    if(it == st_auth.end()) return false;
    
    totp g(it->second);
    data = g.get();
    return true;
}

bool Manager::del_auth(const std::string &name){
    auto it = st_auth.find(name);
    if(it == st_auth.end()) return false;

    st_auth.erase(it);
    sync(no_sync);
    return true;
}

bool Manager::add_auth(const std::string &name, const totp_data_t &data){
    auto it = st_auth.find(name);
    if(it != st_auth.end()) return false;
    
    st_auth[name] = data;
    stale_storage = true;
    sync(no_sync);
    return true;
}
