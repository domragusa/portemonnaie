// #include <iostream>

#include "secstorage.hpp"

#include <sstream>
#include <cryptopp/hkdf.h>
#include <cryptopp/sha.h>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/files.h>
#include <cryptopp/filters.h>

using byte = CryptoPP::byte;
using HKDF_SHA256 = CryptoPP::HKDF<CryptoPP::SHA256>;
// using SecByteBlock = CryptoPP::SecByteBlock;
using CFB_AES = CryptoPP::CFB_Mode<CryptoPP::AES>;
using StringSource = CryptoPP::StringSource;
using StringSink = CryptoPP::StringSink;
using StreamTransformationFilter = CryptoPP::StreamTransformationFilter;
using FileSink = CryptoPP::FileSink;
using FileSource = CryptoPP::FileSource;


SecStorage::SecStorage(const std::string path) :
    key(CryptoPP::AES::MAX_KEYLENGTH),
    iv(CryptoPP::AES::BLOCKSIZE),
    path(path)
{
}

const char * salt = "T2JtX7QpLyGtTAsWCkqKPVsBMbzUkMYJ9s9uAJWB5fz9RAYYVY";
void SecStorage::derive_key(const std::string &password){
    HKDF_SHA256 hkdf;
    hkdf.DeriveKey(
        key, key.size(),
        (byte*)password.c_str(), password.size(),
        (byte*)salt, sizeof(salt),
        (byte*)"KEY", 3 //info
    );
    hkdf.DeriveKey(
        iv, iv.size(),
        (byte*)password.c_str(), password.size(),
        (byte*)salt, sizeof(salt),
        (byte*)"VEC", 3 //info
    );
}

void SecStorage::write_crypted(const std::string &data){
    CFB_AES::Encryption enc;
    try{
        enc.SetKeyWithIV(key, key.size(), iv);
        StringSource(data, true,
                new StreamTransformationFilter(enc, 
                new FileSink(path.c_str())
                )
        );
    }catch(CryptoPP::Exception& e){}
}

void SecStorage::read_crypted(std::string &data){
    CFB_AES::Decryption dec;
    try{
        dec.SetKeyWithIV(key, key.size(), iv);
        FileSource(path.c_str(), true, 
                new StreamTransformationFilter(dec,
                new StringSink(data)
                )
        );
    }catch(CryptoPP::Exception& e){}
}
    
void SecStorage::load(passmap_t &st_pass, totpmap_t &st_auth){
    std::string buffer;
    std::stringstream file;
    
    read_crypted(buffer);
    file << buffer;
    
    st_pass.clear();
    while(file.good()){
        std::string line, name, pass;
        
        std::getline(file, line);
        std::stringstream tmp(line);
        
        tmp >> name;
        std::getline(tmp, pass);
        if(name.size()==0 || pass.size()==0) break;
        
        st_pass[name] = std::move(pass);
    }
    
    st_auth.clear();
    while(file.good()){
        std::string line, name, token;
        unsigned int digits, interval;
        
        std::getline(file, line);
        std::stringstream tmp(line);
        
        tmp >> name >> digits >> interval;
        std::getline(tmp, token);
        if(name.size()==0 || token.size()==0) break;
        
        totp_data_t data;
        data.b32_key = std::move(token);
        data.digits = digits;
        data.interval = interval;
        st_auth[name] = std::move(data);
    }
    
}


void SecStorage::store(const passmap_t &st_pass, const totpmap_t &st_auth){
    std::stringstream buf;
    
    for(auto it=st_pass.begin(); it != st_pass.end(); ++it){
        buf << it->first << " " << it->second << "\n";
    }
    
    buf << "\n";
    
    for(auto it=st_auth.begin(); it != st_auth.end(); ++it){
        const totp_data_t &tmp = it->second;
        buf << it->first << " "
            << tmp.digits << " "
            << tmp.interval << " "
            << tmp.b32_key << " "
            << "\n";
    }
    
    write_crypted(buf.str());
}
