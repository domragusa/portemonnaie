#include <iostream>
#include <string>
#include <cstdlib>

#include "manager.hpp"
#include "dbus-interface.hpp"

static int helper_del_pass(msg_t *msg, void *data, err_t *err){
    if(data == nullptr) return -1;
    
    std::string name;
    int r = dbus::read_msg_str(msg, name);
    if(r < 0) return -1;
    
    return dbus::reply_bool(msg, ((Manager*)data)->del_pass(name));
}

static int helper_add_pass(msg_t *msg, void *data, err_t *err){
    if(data == nullptr) return -1;
    
    std::string name, pass;
    int r = 0;
    r |= dbus::read_msg_str(msg, name);
    r |= dbus::read_msg_str(msg, pass);
    if(r < 0) return -1;
    
    return dbus::reply_bool(msg, ((Manager*)data)->add_pass(name, pass));
}

static int helper_get_pass(msg_t *msg, void *data, err_t *err){
    if(data == nullptr) return -1;
    
    std::string name, pass;
    int r = dbus::read_msg_str(msg, name);
    if(r < 0) return -1;
    
    if(!((Manager*)data)->get_pass(name, pass)){
        dbus::set_error(err, "notfound", "impossible to find specified data");
        return -1;
    }
    
    return dbus::reply_str(msg, pass);
}

static int helper_del_auth(msg_t *msg, void *data, err_t *err){
    if(data == nullptr) return -1;
    
    std::string name;
    int r = dbus::read_msg_str(msg, name);
    if(r < 0) return -1;
    
    return dbus::reply_bool(msg, ((Manager*)data)->del_auth(name));
}

static int helper_add_auth(msg_t *msg, void *data, err_t *err){
    if(data == nullptr) return -1;
    
    std::string name, key;
    u32_t interval, digits;
    totp_data_t tmp;
    int r = 0;
    r |= dbus::read_msg_str(msg, name);
    r |= dbus::read_msg_str(msg, key);
    r |= dbus::read_msg_u32(msg, interval);
    r |= dbus::read_msg_u32(msg, digits);
    if(r < 0) return -1;
    tmp.b32_key = std::move(key);
    tmp.interval = interval;
    tmp.digits = digits;
    
    return dbus::reply_bool(msg, ((Manager*)data)->add_auth(name, tmp));
}

static int helper_get_auth(msg_t *msg, void *data, err_t *err){
    if(data == nullptr) return -1;
    
    std::string name, token;
    int r = dbus::read_msg_str(msg, name);
    if(r < 0) return -1;
    
    if(!((Manager*)data)->get_auth(name, token)){
        dbus::set_error(err, "notfound", "impossible to find specified data");
        return -1;
    }
    
    return dbus::reply_str(msg, token);
}

const auto UNPRIV = SD_BUS_VTABLE_UNPRIVILEGED;
static const sd_bus_vtable serv_vtable[] = {
SD_BUS_VTABLE_START(0),

// SD_BUS_METHOD("initKey", "s", "b", helper_init_key, UNPRIV),
// SD_BUS_METHOD("changeKey", "s", "b", helper_change_key, UNPRIV),

SD_BUS_METHOD("delPass", "s", "b", helper_del_pass, UNPRIV),
SD_BUS_METHOD("getPass", "s", "s", helper_get_pass, UNPRIV),
SD_BUS_METHOD("addPass", "ss", "b", helper_add_pass, UNPRIV),

SD_BUS_METHOD("delAuth", "s", "b", helper_del_auth, UNPRIV),
SD_BUS_METHOD("getAuth", "s", "s", helper_get_auth, UNPRIV),
SD_BUS_METHOD("addAuth", "ssuu", "b", helper_add_auth, UNPRIV),

SD_BUS_VTABLE_END
};

int main(int argc, char *argv[]) {
    Manager man("./pass.db");
    man.init_key(std::string("test123"));
    dbus bus("/", "io.gitlab.drtiny.test", serv_vtable);
    int r;

    bus.set_userdata(&man);
    while(true){
        r = bus.process();
        if(r < 0){
            std::cerr << "BUS, unable to process: " << std::strerror(-r) << "\n";
            break;
        }else if(r > 0) continue;

        if (bus.wait() < 0) {
            std::cerr << "BUS, unable to wait: " << std::strerror(-r) << "\n";;
            break;
        }
    }

    return 0;
}
