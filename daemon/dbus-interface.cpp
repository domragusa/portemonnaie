#include "dbus-interface.hpp"

dbus::dbus(const std::string &path, const std::string &name, const vtable_t *table) : bus(nullptr), slot(nullptr), ready(false) {
    if(table == nullptr) return;
    if(sd_bus_open_user(&bus) < 0) return;
    if(sd_bus_add_object_vtable(bus, &slot, path.c_str(), name.c_str(), table, nullptr) < 0) return;
    if(sd_bus_request_name(bus, name.c_str(), 0) < 0) return;
    
    ready = true;
}

dbus::~dbus(){
    sd_bus_slot_unref(slot);
    sd_bus_unref(bus);
}

bool dbus::is_ready(){
    return ready;
}

void* dbus::set_userdata(void *data){
    if(!is_ready()) return nullptr;
    return sd_bus_slot_set_userdata(slot, data);
}

int dbus::process(){
    if(!is_ready()) return -1;
    return sd_bus_process(bus, nullptr);
}

int dbus::wait(){
    if(!is_ready()) return -1;
    return sd_bus_wait(bus, (uint64_t)-1);
}

int dbus::read_msg_str(msg_t *msg, std::string &out){
    char *buf;
    int r = sd_bus_message_read(msg, "s", &buf);
    out = buf;
    return r;
}

int dbus::read_msg_u32(msg_t *msg, u32_t &buf){
    return sd_bus_message_read(msg, "u", &buf);
}

int dbus::reply_str(msg_t *msg, const std::string &data){
    return sd_bus_reply_method_return(msg, "s", data.c_str());
}

int dbus::reply_bool(msg_t *msg, bool data){
    return sd_bus_reply_method_return(msg, "b", data);
}

int dbus::set_error(err_t *err, const std::string &name, const std::string &text){
    return sd_bus_error_set_const(err, name.c_str(), text.c_str());
}
