#ifndef DR_TOTP_HPP
#define DR_TOTP_HPP

#include <cryptopp/secblock.h>
#include <cryptopp/hmac.h>
#include <cryptopp/sha.h>

#include <string>
#include <cinttypes>


using u32_t = std::uint32_t;
using u64_t = std::uint64_t;

using HMAC_SHA1 = CryptoPP::HMAC<CryptoPP::SHA1>;
using SecByteBlock = CryptoPP::SecByteBlock;
using byte = CryptoPP::byte;

struct totp_data_t{
    std::string b32_key;
    int digits;
    int interval;
};

class totp{
private:
    HMAC_SHA1 hmac;
    u32_t digits;
    u32_t interval;
    
    void calc_hmac(const byte buf[8], byte out[20]);
    std::string add_padding(const int value);
    
public:
    totp(const SecByteBlock &key, const int digits=6, const int interval=30);
    totp(const totp_data_t &data);
    
    std::string get();
};

#endif
