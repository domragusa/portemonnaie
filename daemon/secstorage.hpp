#ifndef DR_SECSTORAGE_HPP
#define DR_SECSTORAGE_HPP

#include <string>
#include <map>
#include <cinttypes>
#include <cryptopp/secblock.h>

#include "totp.hpp"

using byte = CryptoPP::byte;
using SecByteBlock = CryptoPP::SecByteBlock;
using u32_t = std::uint32_t;

using passmap_t = std::map<std::string, std::string>;
using totpmap_t = std::map<std::string, totp_data_t>;


class SecStorage{
private:
    SecByteBlock key;
    SecByteBlock iv;
    std::string path;
    
    void write_crypted(const std::string &data);
    void read_crypted(std::string &data);
    
public:
    SecStorage(std::string path);
    void derive_key(const std::string &password);
    
    void load(passmap_t &st_pass, totpmap_t &st_auth);
    void store(const passmap_t &st_pass, const totpmap_t &st_auth);
};

#endif
