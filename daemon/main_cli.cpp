#include <iostream>
#include <string>
#include <cstdlib>

#include "manager.hpp"

void add_action(Manager &m){
    std::string id, pass;
    
    std::cout << "\n\nSet password:\n";
    std::cout << "ID:   ";
    std::cin >> id;
    std::cout << "Pass: ";
    std::cin >> pass;
    
    m.add_pass(id, pass);
}

void get_action(Manager &m){
    std::string id, pass;
    
    std::cout << "\n\nGet password:\n";
    std::cout << "ID:   ";
    std::cin >> id;
    
    if(m.get_pass(id, pass)){
        std::cout << "Pass: " << pass << "\n";
    }else{
        std::cout << "\nNOT FOUND!\n";
    }
}

void add_a_action(Manager &m){
    std::string id, key;
    int digits, interval;
    
    std::cout << "\n\nAdd auth:\n";
    std::cout << "ID:       ";
    std::cin >> id;
    std::cout << "Key:      ";
    std::cin >> key;
    std::cout << "Digits:   ";
    std::cin >> digits;
    std::cout << "Interval: ";
    std::cin >> interval;
    
    totp_data_t tmp;
    tmp.b32_key = std::move(key);
    tmp.digits = digits;
    tmp.interval = interval;
    m.add_auth(id, tmp);
}

void get_a_action(Manager &m){
    std::string id, token;
    
    std::cout << "\n\nGet token:\n";
    std::cout << "ID:    ";
    std::cin >> id;
    
    if(m.get_auth(id, token)){
        std::cout << "Token: " << token << "\n";
    }else{
        std::cout << "\nNOT FOUND!\n";
    }
}


int main(int argc, char** argv){
    Manager man("./pass.db");
    man.init_key(std::string("test123"));
    
    std::string cmd;
    while(std::cin){
        std::cout << "> ";
        cmd = "";
        std::cin >> cmd;
        if(cmd == "get_pass"){
            get_action(man);
        }else if(cmd == "add_pass"){
            add_action(man);
        }else if(cmd == "get_auth"){
            get_a_action(man);
        }else if(cmd == "add_auth"){
            add_a_action(man);
        }
        else{
            break;
        }
    }
    std::cout << "\n";
    return 0;
}
