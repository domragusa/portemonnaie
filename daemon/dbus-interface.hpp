#ifndef DR_DBUS_INTERFACE_HPP
#define DR_DBUS_INTERFACE_HPP

#include <string>
#include <cinttypes>
#include <systemd/sd-bus.h>

using msg_t = sd_bus_message;
using err_t = sd_bus_error;
using vtable_t = sd_bus_vtable;
using u32_t = std::uint32_t;

// this is a very light wrapper around sdbus, nothing fancy
class dbus{
private:
    sd_bus *bus;
    sd_bus_slot *slot;
    bool ready;
public:
    dbus(const std::string &path, const std::string &name, const vtable_t *table);
    ~dbus();
    bool is_ready();
    void* set_userdata(void *data);
    int process();
    int wait();
    
    // static helpers, just a little sugar
    static int read_msg_str(msg_t *msg, std::string &out);
    static int read_msg_u32(msg_t *msg, u32_t &buf);
    static int reply_str(msg_t *msg, const std::string &data);
    static int reply_bool(msg_t *msg, bool data);
    static int set_error(err_t *err, const std::string &name, const std::string &text);
};

#endif
